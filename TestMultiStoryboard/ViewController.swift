//
//  ViewController.swift
//  TestMultiStoryboard
//
//  Created by Michel Jackson on 2/12/16.
//  Copyright © 2016 Sensimetrics. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    // MARK: Actions
    @IBAction func topic1Button(sender: UIButton) {
        print("topic1Button")
        
        var storyboard = UIStoryboard(name: "Topic1", bundle: nil)
        var controller = storyboard.instantiateViewControllerWithIdentifier("topic1Navigation") as UIViewController
        
        self.presentViewController(controller, animated: true, completion: nil)
    }
    
    @IBAction func unwindFromTopic2(unwindSegue: UIStoryboardSegue) {
        
    }
    
}

